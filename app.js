// 2. Skapa en <ul> tagg i din HTML-fil och skapa sedan en array med fem frukter i din JS-fil.

// a. Tilldela ul-taggen ett id som indikerar att det är en lista.

// b. Skapa nu en variabel som du tilldelar ul-taggen med hjälp av getElementById.

// c. Vilken for-loop ska du använda för att loopa igenom en lista? Skapa den.

// d. Skapa en ny variabel i loopen som du döper till item.

// e. Vilken metod behöver du använda för att skapa ett <li> element? Tilldela den din item variabel.

// f. Med hjälp av innerHTML kan du nu tilldela din item variabel med frukter.

//nivå1

 let text=document.getElementById("title")
 console.log(text)

 const namn=document.createElement("div")
 namn.innerHTML="my name is Zohreh."
 document.body.append(namn)

 //c

 text.remove()
 
 //d

 namn.append(text)

 //e

 text.innerHTML="This is my page."

 //g

 text.className="text"
 text.classList.add("newClass")

 //2

 const fruktArray= ["Äpple", "apelsin","banan","vindruvor","päron"]
 const frukList= document.getElementById("fruktList")

 for (const frukt of fruktArray) {

    const listitem= document.createElement("li")
    listitem.innerHTML= frukt

    frukList.append(listitem)    
 }


//  1. Skapa en bild-mapp som du lägger in fem bilder i.

// a. Hur kan du nu se till att tilldela en variabel med en av bilderna?

// b. Gör en loop som har för avsikt att visa varje bild i mappen.

// c. Skapa css som gör att bilderna hamnar max två på rad (använd flex eller grid)

// d. Gör ett konditionsvilkor som kollar om en bild är för stor så får den inte komma med in i loopen.

//Nivå2

//a
// const image= new Image
//  image.src="pic/anita-austvika-KVhq4ihbhtM-unsplash.jpg"

//  document.body.append(image)
//  image.className="myimg"


 //b
 const img1=new Image(100,200)
img1.src="pic/anita-austvika-KVhq4ihbhtM-unsplash.jpg"
  const img2 =new Image(200,300)
  //  image2.setAttribute("src","pic/dmitry-ganin-7PsXcT5pxCM-unsplash.jpg")
  img2.src="pic/dmitry-ganin-7PsXcT5pxCM-unsplash.jpg"



 const img3=new Image(200,300)
 //  image3.setAttribute("src","pic/keith-tanner-xUDHVUJia18-unsplash.jpg")
 img3.src="pic/keith-tanner-xUDHVUJia18-unsplash.jpg"


const img4=new Image(100,200)
//  image4.setAttribute("src","pic/milad-fakurian-zGlqDAMI1OA-unsplash.jpg")
img4.src="pic/milad-fakurian-zGlqDAMI1OA-unsplash.jpg"






// const imagediv=document.createElement("div")
// document.body.append(imagediv)
// imagediv.className="imgcontainer"

//  const image1= new image("")
//  image1.src="pic/anita-austvika-KVhq4ihbhtM-unsplash.jpg"
//  const image2= document.createElement("img")
//  image2.setAttribute("src","pic/dmitry-ganin-7PsXcT5pxCM-unsplash.jpg")
//  const image3= document.createElement("img")
//  image3.setAttribute("src","pic/keith-tanner-xUDHVUJia18-unsplash.jpg")
//  const image4= document.createElement("img")
//  image4.setAttribute("src","pic/milad-fakurian-zGlqDAMI1OA-unsplash.jpg")
//  const image5= document.createElement("img")
//  image5.setAttribute("src","pic/venti-views-CfYAuZgvhpY-unsplash.jpg")

 

 const myimages= [img1,img2,img3,img4]
 
 for (let image of myimages) {
   if (image.width>100) {
      document.body.appendChild(image)
   }  
 }

 
 
 








